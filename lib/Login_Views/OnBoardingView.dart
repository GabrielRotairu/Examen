import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:untitled/Custom_Views/EkInputText.dart';

import '../Firebase_Objects/Perfil.dart';

class OnBoardingView extends StatelessWidget {

  OnBoardingView({Key? key}) : super(key: key);

  FirebaseFirestore db = FirebaseFirestore.instance;

  EKinput_text inputNombre = EKinput_text(
    sTitulo: "Nombre",
    lText: 50,
  );
  EKinput_text inputApellido = EKinput_text(
    sTitulo: "Apellido",
    lText: 50,
  );

// Este método introduce los nuevos datos a la base de Datos
  void btnAceptar(String Nombre, String Apellido, context) async {
    Perfil perfil = new Perfil(
      nombre: Nombre,
      apellido: Apellido,
    );
   await db
        .collection("perfiles")
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .set(perfil.toFirestore())
        .onError((e, _) => print("Error on writing document : $e"));
    Navigator.of(context).popAndPushNamed('/Home');
    print("hola");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('On Boarding'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            inputNombre,
            inputApellido,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  onPressed: () {
                    btnAceptar(inputNombre.getText(),inputApellido.getText(),context);
                    print("DEBUG:             "+inputNombre.getText()+"|"+inputApellido.getText());
                  },
                  child: Text('Crear Usuario'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).popAndPushNamed('/Login');
                  },
                  child: Text('Cancelar'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
