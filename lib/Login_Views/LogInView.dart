import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:untitled/Custom_Views/EkInputText.dart';

EKinput_text inputNombre = EKinput_text(
  blIsPassword: false,
  sTitulo: "User",
  lText: 40,
);
EKinput_text inputPassword = EKinput_text(
  blIsPassword: true,
  sTitulo: "Password",
  lText: 10,
);

class LogInView extends StatelessWidget {
//este método permite comprobar que el usuario con el que queremos iniciar sesión
// existe en nuestra base de datos
  void btnlog(BuildContext context) async {
    print("--------------->" + inputNombre.getText());
    try {
      final credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: inputNombre.getText(),
        password: inputPassword.getText(),
      );
      Navigator.of(context).popAndPushNamed('/onBoarding');
    } on FirebaseAuthException catch (e) {
      print("------->>>>   ERROR DE CREACION DE USUARIO " + e.code);
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
    print("Bienvenido: "+inputNombre.getText());

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text("Log In"), backgroundColor: Colors.red),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Flexible(child: inputNombre),
        Flexible(child: inputPassword),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                btnlog(context);
              },
              child: Text('Log In'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).popAndPushNamed('/Register');
              },
              child: Text('Sign In'),
            ),

          ],
        ),
      ]),
    );
  }
}
