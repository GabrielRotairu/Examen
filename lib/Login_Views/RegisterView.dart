import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:untitled/Custom_Views/EkInputText.dart';

class RegisterView extends StatelessWidget {
  RegisterView({Key? key}) : super(key: key);
//Este método nos va a permitir crear/registrar un nuevo usuario en nuestra app
  void btnRegister(
      String Correo, String password, BuildContext context) async {
    try {
      final credential =
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: Correo,
        password: password,
      );
    } on FirebaseAuthException catch (e) {
      print("------->>>>   ERROR DE CREACION DE USUARIO " + e.code);
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }

    print("Usuario creado correctamente :) ");
  }

  @override
  Widget build(BuildContext context) {
    EKinput_text input1 = EKinput_text(
      sTitulo: "Create User",
      lText: 40,
    );
    EKinput_text input2 = EKinput_text(
      sTitulo: " Create Password",
      blIsPassword: true,
      lText: 10,
    );
    EKinput_text input3 = EKinput_text(
      sTitulo: " Confirm Password",
      blIsPassword: true,
      lText: 10,
    );
    return Scaffold(
      appBar: AppBar(
        title:  Text('Register'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child:
        Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          input1,
          input2,
          input3,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                onPressed: () {
                  if (input2.getText() == input3.getText()) {
                    btnRegister(input1.getText(), input2.getText(), context);
                  }
                  Navigator.of(context).popAndPushNamed('/onBoarding');
                },
                child: Text('Registrarse'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).popAndPushNamed('/Login');
                },
                child: Text('Cancelar'),
              ),
            ],
          )
        ],
        ),
      ),
    );
  }
}
