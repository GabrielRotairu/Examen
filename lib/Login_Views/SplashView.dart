import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SplashView extends StatefulWidget {
  final String sLogoPath;

  const SplashView(this.sLogoPath, {super.key});

  @override
  State<StatefulWidget> createState() {
    return _SplashView();

  }
}

class _SplashView extends State<SplashView> {
//Este método nos permite ver si el usuario ya esta loggeado que le mande al home
// y si no lo está que le mande al LogIn para que le meta  su cuenta.
  void isUserLogged() async {
    await Future.delayed(Duration(seconds: 3));
    if (FirebaseAuth.instance.currentUser == null) {
      Navigator.of(context).popAndPushNamed("/Login");
    } else {
      if (FirebaseAuth.instance.currentUser !=null) {
        Navigator.of(context).popAndPushNamed("/Home");
      }
    }
  }


//Con este método podemos cargar los datos de Firebase y hacer que
  //nuestro método para ver si el usuario ya está loggeado o no
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isUserLogged();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.red,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(image: AssetImage(widget.sLogoPath), width: 200),
              Padding(padding: EdgeInsets.all(20)),
              Text("Cargando..."),
              Padding(padding: EdgeInsets.all(20)),
              const CircularProgressIndicator(
                semanticsLabel: 'Circular progress indicator',
                color: Colors.white,
              )
            ],
          ),
        ));
  }
}
