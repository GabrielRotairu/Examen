import 'package:cloud_firestore/cloud_firestore.dart';

class Perfil {
  late final String? nombre;
  late final String? apellido;




  Perfil({
    this.nombre = "",
    this.apellido = "",

  });

  factory Perfil.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options,
      ) {
    final data = snapshot.data();
    return Perfil(
      nombre: data?['Nombre'],
      apellido: data?['Apellido'],


    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (apellido != null) "Apellido": apellido,
      if (nombre != null) "Nombre": nombre,

    };
  }
}
