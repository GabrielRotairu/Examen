import 'package:cloud_firestore/cloud_firestore.dart';

class Miembro {
  late final int? Edad;
  late final String? Nombre;
  late final String? Procedencia;
  late final String? Rango;




  Miembro({
    this.Edad,
    this.Nombre = "",
    this.Procedencia = "",
    this.Rango="",

  });

  factory Miembro.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options,
      ) {
    final data = snapshot.data();
    return Miembro(
      Edad: data?['Edad'],
      Nombre: data?['Nombre'],
      Procedencia: data?['Procedencia'],
      Rango: data?['Rango'],


    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (Edad != null) "Edad": Edad,
      if (Nombre != null) "Nombre": Nombre,
      if (Procedencia != null) "Procedencia": Procedencia,
      if (Rango != null) "Rango": Rango,


    };
  }
}
