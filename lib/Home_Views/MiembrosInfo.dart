import 'package:flutter/material.dart';
import 'package:untitled/App.dart';

import '../Singleton/DataHolder.dart';

class MiembrosInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.red,
          title: Text(DataHolder().miembro.Nombre!)),
      body: Center(
        child: Column(
          children: [
            Padding(padding: EdgeInsets.all(10)),
            ImageIcon(AssetImage("Assets/Images/Logo1.png"), size: 150),
            Padding(padding: EdgeInsets.all(10)),
            Container(
              child: Text("RANGO:  " + DataHolder().miembro.Rango!,
                  style: TextStyle(color: Colors.white, fontSize: 40)),
              color: Colors.red,
              width: 360,
            ),
            Padding(padding: EdgeInsets.all(20)),
            Container(
              child: Text("EDAD:   " + DataHolder().miembro.Edad.toString(),
                  style: TextStyle(color: Colors.white, fontSize: 40)),
              color: Colors.red,
              width: 360,
            ),
            Padding(padding: EdgeInsets.all(20)),
            Container(
              child: Text("PROCEDENCIA:   " + DataHolder().miembro.Procedencia!,
                  style: TextStyle(color: Colors.white, fontSize: 40)),
              color: Colors.red,
              width: 360,
            ),
            Padding(padding: EdgeInsets.all(10)),
          ],
        ),
      ),
    );
  }
}
