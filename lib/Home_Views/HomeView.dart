import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:untitled/Custom_Views/RFButton.dart';
import 'package:untitled/Firebase_Objects/Miembro.dart';
import 'package:untitled/ListItems/Miembros.dart';
import 'package:untitled/Singleton/DataHolder.dart';

RFButton btn1 = RFButton();

class HomeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeView();
  }
}

class _HomeView extends State<HomeView> {
  List<Miembro> MiembroList = [];
  FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();
    actualizarListas();
  }
//Este método nos permite descargar los elementos de la base de datos e introducirlos al ListView
  void actualizarListas() async {
    final ref = db.collection("Miembros").orderBy("Nombre").withConverter(
          fromFirestore: Miembro.fromFirestore,
          toFirestore: (Miembro mb, _) => mb.toFirestore(),
        );
    final docSnap = await ref.get();

    setState(() {
      for (int i = 0; i < docSnap.docs.length; i++) {
        MiembroList.add(docSnap.docs[i].data());
      }
    });
  }
//Este método nos permite saber a que ítem de la lista estamos accediendo y que nos dirija a la información de ese ítem
  void ItemShortClick(int index) {
    print("DEBUG:   " + index.toString());
    print("DEBUG:   " + MiembroList[index].Nombre!);
    DataHolder().miembro=MiembroList[index];
    Navigator.pushNamed(context, "/MiembrosInfo");

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child:
        ListView.separated(
          padding: EdgeInsets.all(10),
          itemCount: MiembroList.length,

          itemBuilder: (context, index) {
            return Miembros(
                sTitulo: MiembroList[index].Nombre!,
                onShortClick: ItemShortClick,
                index: index);
          },
          separatorBuilder: (BuildContext context, int index) {
            return const Divider();
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).popAndPushNamed('/addMiembro');
        },
        label: Text("Add"),
        backgroundColor: Colors.red,
        icon:  ImageIcon(AssetImage("Assets/Images/Logo1.png")),
      ),
    );
  }
}
