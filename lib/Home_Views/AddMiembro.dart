import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:untitled/Custom_Views/EkInputText.dart';
import '../Firebase_Objects/Miembro.dart';
//Agregamos los inputTexts necesarios para recoger los datos
EKinput_text inputNombre = EKinput_text(
  sTitulo: "Nombre",
  lText: 30,
);
EKinput_text inputEdad = EKinput_text(
  sTitulo: "Edad",
  lText: 2,
);
EKinput_text InputProcedencia = EKinput_text(
  sTitulo: "Procedencia",
  lText: 30,
);
EKinput_text InputRango = EKinput_text(
  sTitulo: "Rango",
  lText: 20,
);

class AddMiembro extends StatelessWidget {
  FirebaseFirestore db = FirebaseFirestore.instance;
// Con este método podemos añadir un nuevo miembro a nuestra colección
  void btnAceptar(int Edad, String Nombre, String Procedencia, String Rango,
      context) async {
    Miembro mb = new Miembro(
      Edad: int.parse(inputEdad.getText()),
      Nombre: inputNombre.getText(),
      Procedencia: InputProcedencia.getText(),
      Rango: InputRango.getText(),
    );

    final docRef = db.collection("Miembros");

    await docRef.add(mb.toFirestore());
    Navigator.of(context).popAndPushNamed('/Home');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text("Nuevo Miembro"), backgroundColor: Colors.red),
      body: Center(
        child: Column(
          children: [
            Flexible(child: inputEdad),
            Flexible(child: inputNombre),
            Flexible(child: InputProcedencia),
            Flexible(child: InputRango),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              OutlinedButton(
                  onPressed: () {
                    btnAceptar(
                        int.parse(inputEdad.getText()),
                        inputNombre.getText(),
                        InputProcedencia.getText(),
                        InputRango.getText(),
                        context);
                  },
                  child: Text("Agregar", style: TextStyle(color: Colors.white)),
                  style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.red),
                  )),
            ])
          ],
        ),
      ),
    );
  }
}
