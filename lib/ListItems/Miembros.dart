import 'package:flutter/material.dart';

class Miembros extends StatelessWidget {
  final String sTitulo;
  final Function(int Index) onShortClick;
  final int index;

  Miembros(
      {Key? key,
        this.sTitulo = "Title",
        required this.onShortClick,
        required this.index,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListTile(
      title: Text(sTitulo),
      leading: ImageIcon(AssetImage("Assets/Images/Logo1.png")),
      onTap: () {
        onShortClick(index);
      },
    );
  }


}
