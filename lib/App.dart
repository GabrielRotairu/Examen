import 'package:flutter/material.dart';
import 'package:untitled/Home_Views/HomeView.dart';
import 'package:untitled/Home_Views/MiembrosInfo.dart';
import 'package:untitled/Login_Views/OnBoardingView.dart';
import 'package:untitled/Login_Views/RegisterView.dart';
import 'package:untitled/Login_Views/SplashView.dart';

import 'Home_Views/AddMiembro.dart';
import 'Login_Views/LogInView.dart';

class App extends StatelessWidget {
  App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      initialRoute: '/Splash',
      routes: {
        '/Login': (context) => LogInView(),
        '/Register' :(context)=>RegisterView(),
        '/Splash': (context) => SplashView("Assets/Images/Logo1.png"),
        '/Home': (context) => HomeView(),
        '/onBoarding':(context) => OnBoardingView(),
        '/MiembrosInfo':(context) => MiembrosInfo(),
        '/addMiembro':(context) => AddMiembro(),


      },
    );
  }
}
