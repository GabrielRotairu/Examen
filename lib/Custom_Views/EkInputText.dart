import 'package:flutter/material.dart';

class EKinput_text extends StatelessWidget {
  final String sTitulo;
  final bool blIsPassword;
  final int lText;
  final TextEditingController myController = TextEditingController(text: "");

  EKinput_text(
      {Key? key,
      this.sTitulo = "",
      this.blIsPassword = false,
      this.lText = 10,
      })
      : super(key: key);

  String getText() {
    return myController.text;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: blIsPassword,
      controller: myController,
      cursorColor: Colors.red,
      maxLength: lText,
      focusNode: FocusNode(descendantsAreFocusable: true),
      decoration: InputDecoration(
        labelText: sTitulo,
        labelStyle: TextStyle(color: Colors.black),
        prefixIcon:Padding(
        padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
    child: Image(
          image: AssetImage("Assets/Images/Logo1.png"),
          width: 20,
        ),

        ),
      ),
    );
  }
}
